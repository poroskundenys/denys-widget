(function ($) {
    'use strict';
    
    $(document).on('input', '#denys_form input', onFormChange);
    
    function onFormChange(e) {
	e.preventDefault();
	var data   = $('#denys_form').serialize();
	$.ajax({	    
	    url: denys.ajaxurl,
	    method: 'POST',
	    data: data,
	    success: function (res) {
		if (typeof res === 'string') {	
		    
		    $('#denys_list').html(res);
		}		
	    },
	    error: function () {
		console.error(arguments);
	    },
	    
	});
    }
})(jQuery);

