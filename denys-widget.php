<?php

/**
 * Plugin Name: Denys widget
 * Description: Denys widget
 * Version: 1
 * Author:      Poroskun Denys
 * Text Domain: widget
 * Domain Path: /languages/
 */
class DenysWidget extends WP_Widget {

    function __construct() {
        parent::__construct(
        'denys_widget',
        __( 'Denys widget', 'widget' ),
            array( 'description' => __( 'Denys widget description', 'widget' ) )
        );
    }

    public function widget( $args, $instance ) {
        $title          = apply_filters( 'widget_title', $instance[ 'title' ] );
        $posts_per_page = $instance[ 'posts_per_page' ];

        echo $args[ 'before_widget' ];

        if ( ! empty( $title ) )
            echo $args[ 'before_title' ] . $title . $args[ 'after_title' ];

        $q = new WP_Query( "posts_per_page=$posts_per_page&&post_type=post" );
        if ( $q->have_posts() ):
            ?>
            <form id="denys_form">
                <input type="text" name="denys_title" value="" placeholder="<?php esc_html_e( 'Search tittle', 'widget' ); ?>"/>
                <input type="date" name="denys_date" value="" placeholder="<?php esc_html_e( 'Filter date', 'widget' ); ?>"/>
                <input type="hidden" name="action" value="denys_form"/>
            </form>
            <ul id="denys_list">
                <?php
                while ( $q->have_posts() ): $q->the_post();
                    ?>
                    <li><a href="<?php the_permalink() ?>"><?php the_title() ?></a></li>
                    <?php
                endwhile;
                ?>
            </ul>
            <?php
        endif;
        wp_reset_postdata();

        echo $args[ 'after_widget' ];
    }

    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        if ( isset( $instance[ 'posts_per_page' ] ) ) {
            $posts_per_page = $instance[ 'posts_per_page' ];
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title', 'widget' ); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'posts_per_page' ); ?>"><?php esc_html_e( 'Posts per page', 'widget' ); ?></label> 
            <input id="<?php echo $this->get_field_id( 'posts_per_page' ); ?>" name="<?php echo $this->get_field_name( 'posts_per_page' ); ?>" type="text" value="<?php echo ($posts_per_page) ? esc_attr( $posts_per_page ) : '5'; ?>" size="3" />
        </p>
        <?php
    }

    public function update( $new_instance, $old_instance ) {
        $instance                     = array();
        $instance[ 'title' ]          = ( ! empty( $new_instance[ 'title' ] ) ) ? strip_tags( $new_instance[ 'title' ] ) : '';
        $instance[ 'posts_per_page' ] = ( is_numeric( $new_instance[ 'posts_per_page' ] ) ) ? $new_instance[ 'posts_per_page' ] : '5';
        return $instance;
    }

}

function denys_widget_load() {
    register_widget( 'DenysWidget' );
}

add_action( 'widgets_init', 'denys_widget_load' );

function denys_register_scripts() {

    wp_enqueue_script( 'denys', plugin_dir_url( __FILE__ ) . 'js/denys.js', array( 'jquery' ), true );
    wp_localize_script( 'denys', 'denys', array(
        'ajaxurl' => admin_url( 'admin-ajax.php' )
    ) );
}

add_action( 'wp_enqueue_scripts', 'denys_register_scripts' );

function denys_form() {
    add_filter( 'posts_search', '__search_by_title_only', 500, 2 );
    $title = $_POST[ 'denys_title' ];
    $date  = $_POST[ 'denys_date' ];
    if ( ! empty( $date ) ) {
        add_filter( 'posts_where', '__filter_where' );
    }
    $q = new WP_Query( "s=$title&post_type=post" );    
    if ( $q->have_posts() ):
        while ( $q->have_posts() ): $q->the_post();
            ?>
            <li><a href="<?php the_permalink() ?>"><?php the_title() ?></a></li>
            <?php
        endwhile;
    else :
        ?>
        <li><?php esc_html_e( 'Not found', 'widget' ); ?></li>
    <?php
    endif;
    die();
}

add_action( 'wp_ajax_denys_form', 'denys_form' );
add_action( 'wp_ajax_nopriv_denys_form', 'denys_form' );

function __search_by_title_only( $search, &$wp_query ) {
    global $wpdb;

    if ( empty( $search ) )
        return $search;

    $q = $wp_query->query_vars;
    $n = ! empty( $q[ 'exact' ] ) ? '' : '%';

    $search    = $searchand = '';

    foreach ( (array) $q[ 'search_terms' ] as $term ) {
        $term      = esc_sql( like_escape( $term ) );
        $search    .= "{$searchand}($wpdb->posts.post_title LIKE '{$n}{$term}{$n}')";
        $searchand = ' AND ';
    }

    if ( ! empty( $search ) ) {
        $search = " AND ({$search}) ";
        if ( ! is_user_logged_in() )
            $search .= " AND ($wpdb->posts.post_password = '') ";
    }

    return $search;
}

function __filter_where( $where = '' ) {
    $date  = $_POST[ 'denys_date' ];    
    $where .= " AND post_date >= '$date'";
    return $where;
}
